<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get("/", "PropertyController@home");

//show the fruitBasket
Route::get("/fruitBasket", "FruitController@showBasket");

//Show and Add a fruit
Route::get("/fruit", "FruitController@showFruitForm");
Route::post("/fruit/new", "FruitController@processFruitForm");

//SHOW AND EDIT FRUIT
Route::get("/fruit/edit/{id}", "FruitController@showFruitDetails");
Route::put("/fruit/edit/{id}", "FruitController@processFruitEdit");

//show one fruit
Route::get("/fruit/{id}", "FruitController@getSingleFruit");

//delete a fruit
Route::get("/pick/{id}", "FruitController@deleteFruit");


//=================STUDENT_ROUTES=====================//
//Show students
Route::get("/studentlist", "StudentController@showStudents");

//Show and add a student
Route::get("/student", "StudentController@showStudentForm");
Route::post("/student/new", "StudentController@processStudentForm");

//SHOW AND EDIT STUDENT
Route::get("/student/edit/{id}", "StudentController@showStudentDetails");
Route::put("/student/edit/{id}", "StudentController@processStudentEdit");

//show students who are taking a specific subject
Route::post("/filterSubject", "StudentController@showSpecificStudents");

//Show ONE student
Route::get("/student/{id}", "StudentController@getSingleStudent");

//DELETE a Single Student
Route::get("/delete/{id}", "StudentController@deleteStudent");

//DELETE ALL STUDENTS
Route::get("/deleteAll", "StudentController@deleteAllStudent");


//=================REAL_ESTATE ROUTES=====================//

Route::get("/home", "PropertyController@home");
Route::get("/properties", "PropertyController@showProperties");
Route::get("/event/{id}", "EventController@showEventsFor");






//==================user_routes=============================//

Route::post("/login", "UserController@login");
Route::get("/login", "UserController@showForm");
Route::get("/logout", "UserController@logout");


Route::get("/register", "UserController@showRegForm");
Route::post("/register", "UserController@signup");

// ============booking==========//

Route::post("/book", "UserTripController@book");


// =====dashboard====//

Route::get("/transactions", "UserTripController@showTransactions");
Route::get("/pendingtransactions", "UserTripController@showPending");
Route::get("/alltransactions", "UserTripContoller@showAll");
Route::post("/confirm", "UserTripController@confirm");


//====adminstuff======//

Route::get("/newproperty", "PropertyController@showForm");
Route::get("/pinfo/{id}", "PropertyController@showInfo");
Route::post("/newproperty", "PropertyController@create");
Route::post("/deleteproperty", "PropertyController@delete");


Route::get("/newevent", "EventController@showForm");
Route::get("/einfo/{id}", "EventController@showInfo");
Route::post("/newevent", "EventController@create");
Route::get("/event/delete/{id}", "EventController@delete");




