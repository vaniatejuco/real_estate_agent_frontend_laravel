<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;

class UserController extends Controller
{
    public function showForm () {
    
    	return view("/real_estate/login");
    }
    public function showRegForm () {
    
    	return view("/real_estate/register");
    }
    
    public function login (Request $request) {
       
		$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

		//POST = method, /fruits/create = action
		$response = $client->request("POST", "/user/login", [
			"json" => [
				"email" => $request->email,
				"password" => $request->password
			]
        ]);
        
        $result = json_decode($response->getBody());
		// dd($result);
        Session::put("user_id", $result->userId); //user details
    	Session::put("name", $result->name); //user details
    	Session::put("role", $result->role); //user details
    	Session::put("token", "Bearer " . $result->token);

                
        
        return view("real_estate/index");
		// return redirect ("/");
	}
    public function signup (Request $request) {
       
		$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

		//POST = method, /fruits/create = action
		$response = $client->request("POST", "/user/signup", [
			"json" => [
				"name" => $request->name,
				"email" => $request->email,
				"contact_number" => $request->contact_number,
				"password" => $request->password
			]
        ]);
        
        $result = json_decode($response->getBody());
		// dd($result);
        Session::put("message", $result->message); //user details


                
        
        return redirect("/login");
		// return redirect ("/");
	}

	public function logout(){
		Session::flush();
		return redirect("/login");
	}

}
