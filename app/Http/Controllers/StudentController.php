<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client; //Allows us to use guzzle's client to request and receive backend data

class StudentController extends Controller
{
    //============SHOW ALL STUDENT================//
	public function showStudents () {
    	$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

    	$response = $client->request("GET", "/students/showStudents");
    	$result = json_decode($response->getBody());
		// dd($result);

    	$studentslist = $result->result;
    	return view("/student_catalog", compact("studentslist"));
	}

	//============SHOW ONE STUDENT================//
	public function getSingleStudent($id) {
    	$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

    	$response = $client->request("GET", "/students/showAStudent/$id");
    	$result = json_decode($response->getBody());
    	// dd($result);
	}


	//============DELETE STUDENT================//
	public function deleteStudent ($id) {
		$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);
		$response = $client->request("DELETE", "/students/expel/$id");
		return redirect("/studentslist");
	}


	//============DELETE ALL STUDENT================//
	public function deleteAllStudent ($id) {
		$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);
		$response = $client->request("DELETE", "/students/expelAll");
		return redirect("/studentlist");
	}


	//============SHOW ADD A STUDENT FORM================//
	public function showStudentForm () {
		return view("/add_student");
	}


	//============ADD A STUDENT FORM================//
	public function processStudentForm (Request $request) {
    	$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

    	$response = $client->request("POST", "/students/enroll", [
    		"json" => [
    			"name" => $request->name,
    			"year_level" => $request->year_level,
    			"subject1" => $request->subject1,
    			"subject2" => $request->subject2,
    			"subject3" => $request->subject3,
    			"gwa" => $request->gwa,
    		]
		]);
		return redirect("/studentlist");
    }

	//=====SHOW STUDENTS WHO ARE TAKING A SPECIFIC SUBJECT =====//
	public function showSpecificStudents(Request $request) {
		$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);
		$response = $client->request("GET", "/students/subject/".$request->filter);
		$studentslist = json_decode($response->getBody())->result;
		// dd($result);
		// $subject = $result->result[0];
        return view("/student_catalog", compact("studentslist"));
	}


	//============SHOW STUDENT DETAILS================//
	public function showStudentDetails ($id) {
		$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

		$response = $client->request("GET", "/students/showAStudent/$id");
		$result = json_decode($response->getBody());
		$student = $result->result[0];
		return view("/student_info", compact("student"));
	}


	// //============EDIT STUDENT================//
	// public function processStudentEdit($id, Request $request) {
 //    	$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

 //    	$response = $client->request("PUT", "/students/updateAStudent/$id", [
 //    		"json" => [
 //    			"name" => $request->name,
 //    			"year_level" => $request->year_level,
 //    			"subject1" => $request->subject1,
 //    			"subject2" => $request->subject2,
 //    			"subject3" => $request->subject3,
 //    			"gwa" => $request->gwa,
 //    		]
	// 	]);
	// 	return redirect("/studentlist");
	// }		


}
