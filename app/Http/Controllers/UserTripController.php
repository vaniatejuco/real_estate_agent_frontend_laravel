<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as Reqq;
use Session;

class UserTripController extends Controller
{
    public function book(Request $request) {
    	// dd($request);
        $client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);
        $headers = [
        'Authorization' => 'Bearer ' . $request->token,        
        'Accept' => 'application/json',
         ];

		//POST = method, /fruits/create = action
		$response = $client->request("POST", "/usertrip/create", [
            "headers" => $headers,
			"json" => [
				"event_id" => $request->event_id,
				"property_id" => $request->property_id,
				"user_id" => $request->user_id
            ]
		]);
		return redirect ("/transactions");
    }
    public function confirm(Request $request) {
    	// dd($request);
        $client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);
        $headers = [
        'Authorization' => 'Bearer ' . Session::get('token'),        
        'Accept' => 'application/json',
         ];

		//POST = method, /fruits/create = action
        $response = $client->request("PUT", "/usertrip/update/$request->id",
            [
			"json" => [
				"status" => "confirmed",

            ]
		    ]);
        Session::put("message", "trip was confirmed");
		return redirect ("/pendingtransactions");
    }
    
    public function showTransactions() {
        $user_id = Session::get('user_id');
        $headers = ['authorization' => 'Bearer ' . Session::get('token'),  'Accept' => 'application/json'];
    	$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);
    	$response = $client->request("GET", "/usertrip/showByUser/$user_id");
    	$result = json_decode($response->getBody());
		// dd($result);

    	$transactionlist = $result->result;

        return view("/real_estate/userdashboard", compact('transactionlist'));
    }
    public function showPending() {
        
        $headers = ['authorization' => 'Bearer ' . Session::get('token'),  'Accept' => 'application/json'];
    	$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);
    	$response = $client->request("GET", "/usertrip/showByStatus/pending");
    	$result = json_decode($response->getBody());
		// dd($result);

    	$pendinglist = $result->result;

        return view("/real_estate/admindashboard", compact('pendinglist'));
    }
}
