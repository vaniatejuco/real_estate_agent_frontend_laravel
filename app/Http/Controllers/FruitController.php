<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client; //Allows us to use guzzle's client to request and receive backend data

class FruitController extends Controller
{
    public function sample () {
    	$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);
    	//set the default url to localhost:3000	

    	$response = $client->request("GET", "");
    	$message = json_decode($response->getBody()); //->getBody() gets whatever the res.json send to it.
    	dd($message->message);
    }

    //============SHOW ALL FRUIT================//
    public function showBasket () {
    	$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);
        //set the default url to localhost:3000 or heroku to send the request to

    	$response = $client->request("GET", "/fruits/showAll");
        //send the data using the https method as well as the route that it needs

    	$result = json_decode($response->getBody());
        //$response->getBody() contains all the data from the backend server as a JSON

    	// dd($result);
    	$fruitlist = $result->result;
        return view("/catalog", compact("fruitlist"));
    }

	//============SHOW ONE FRUIT================//
    public function getSingleFruit ($id) {
    	$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

    	$response = $client->request("GET", "/fruits/showOne/$id");
    	$result = json_decode($response->getBody());
    	dd($result);
    }

    //============DELETE A SINGLE FRUIT================//
    public function deleteFruit ($id) {
    	$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);
    	$response = $client->request("DELETE", "/fruits/delete/$id");
        return redirect("/fruitBasket");
    }


	//============SHOW ADD FRUIT FORM================//
    public function showFruitForm () {
        return view("/add_fruit");
    }

	//============ADD FRUIT FORM================//
    public function processFruitForm(Request $request) {
    	// dd($request);
		$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

		//POST = method, /fruits/create = action
		$response = $client->request("POST", "/fruits/create", [
			"json" => [
				"name" => $request->name,
				"color" => $request->color,
				"price" => $request->price,
				"taste" => $request->taste
			]
		]);
		return redirect ("/fruitBasket");
	}

	//============SHOW FRUIT DETAILS================//
	public function showFruitDetails ($id) {
		$client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

		$response = $client->request("GET", "/fruits/showOne/$id");
    	$result = json_decode($response->getBody());
    	$fruit = $result->result[0];
    	// dd($result);
        return view("/edit_fruit", compact("fruit"));
	}

	//============EDIT FRUIT================//
    public function processFruitEdit ($id, Request $request) {
        $client = new Client(["base_uri" => "http://blooming-tundra-85462.herokuapp.com/"]);

        $response = $client->request("PUT", "/fruits/update/$id", [
            "json" => [
                "name" => $request->name,
                "color" => $request->color,
                "price" => $request->price,
                "taste" => $request->taste
            ]
        ]);
        return redirect ("/fruitBasket");
    }

}
