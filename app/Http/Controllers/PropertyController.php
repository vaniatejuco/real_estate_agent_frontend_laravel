<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;

class PropertyController extends Controller
{
    public function home () {
    
    	return view("/real_estate/index");
	}
    public function showForm () {
    
    	return view("/real_estate/property_form");
	}
	public function create (Request $request) {
       
		$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

		//POST = method, /fruits/create = action
		$response = $client->request("POST", "/property/create", [
			"json" => [
				"name" => $request->name,
				"address" => $request->address,
				"features" => $request->features,
				"amenities" => $request->amenities,
				"landmarks" => $request->landmarks,
				"total_price" => $request->tcp,
				"monthly" => $request->monthly
			]
        ]);
        
        $result = json_decode($response->getBody());
		// dd($result);
        Session::put("message", "Added New Property"); //user details


                
        
        return redirect("/pendingtransactions");
		// return redirect ("/");
	}

	public function showProperties () {
    	$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

    	$response = $client->request("GET", "/property/showAll");
    	$result = json_decode($response->getBody());
		// dd($result);

    	$propertylist = $result->result;
    	return view("/real_estate/properties", compact("propertylist"));
	}
	public function showInfo ($id) {
    	$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

    	$response = $client->request("GET", "/property/showOne/$id");
    	$result = json_decode($response->getBody());
		// dd($result);

    	$propertylist = $result->result;
    	return view("/real_estate/property", compact("propertylist"));
	}
}
