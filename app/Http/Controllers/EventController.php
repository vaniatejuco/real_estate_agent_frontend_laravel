<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;

class EventController extends Controller
{
    public function showEventsFor ($id) {
    	$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

    	$response = $client->request("GET", "/event/showForProperty/$id");
    	$result = json_decode($response->getBody());
		// dd($result);
        $property_id = $id;
    	$eventlist = $result->result;
    	return view("/real_estate/events", compact("eventlist", "property_id"));
	}
	public function showForm () {
	    $client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

    	$response = $client->request("GET", "/property/showAll");
    	$result = json_decode($response->getBody());
		// dd($result);

    	$propertylist = $result->result;

    
    return view("/real_estate/event_form", compact('propertylist'));
	}
	public function create (Request $request) {
       
		$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

		//POST = method, /fruits/create = action
		$response = $client->request("POST", "/event/create", [
			"json" => [
				"date" => $request->date,
				"property" => $request->property,

			]
		]);	
		
        $result = json_decode($response->getBody());
		// dd($result);
        Session::put("message", "Scheduled a Trip"); //user details


                
        
        return redirect("/pendingtransactions");
		// return redirect ("/");
	}

    public function showInfo ($id) {
    	$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

    	$response = $client->request("GET", "/event/showOne/$id");
    	$result = json_decode($response->getBody());
		// dd($result);
        
    	$eventlist = $result->result;
    	return view("/real_estate/event", compact("eventlist"));
	}
    public function delete ($id) {
    	$client = new Client(["base_uri" => "http://vast-badlands-45419.herokuapp.com/"]);

    	$response = $client->request("DELETE", "/event/delete/$id");
    	$result = json_decode($response->getBody());
		// dd($result);
        
    	
    	return redirect("/pendingtransactions");
	}
}
