<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>@yield("title")</title>

	{{-- Fonts --}}
	<link href="https://fonts.googleapis.com/css?family=Cinzel|Montserrat" rel="stylesheet">

	{{-- Font Awesome --}}
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	{{-- Bootstrap --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
	{{-- cdn boot --}}
	<link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/lux/bootstrap.min.css" rel="stylesheet" integrity="sha384-hVpXlpdRmJ+uXGwD5W6HZMnR9ENcKVRn855pPbuI/mwPIEKAuKgTKgGksVGmlAvt" crossorigin="anonymous">

	{{-- Custom CSS --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>
	{{-- SIDENAV --}}
	{{-- <div class="sidenav">
	    <div class="logo"><img src="https://via.placeholder.com/150x150"></div>
	    <h3 class="info-name">USER</h3>
	    <div class="menu-list">
	        <a href="/studentlist"><i class="fa fa-list"></i> List of Students</a>
	        <a href="/student"><i class="fa fa-plus"></i> Add Student 
	        <a href="/deleteAll"><i class="fas fa-trash"></i>  Delete All Students</a>
	    </div>
	</div> --}}

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">Juan Cruz</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
			@if (Session::has('name'))
			<li class="nav-item"><a class="nav-link disabled" href="#">Welcome, {{ Session::get('name')}}</a></li>
			<li class="nav-item"><a class="nav-link" href="/logout">Logout</a></li>
			@if (Session::get('role') === 1)
			<li class="nav-item"><a class="nav-link" href="/pendingtransactions">Admin Dashboard</a></li>
			@else
			<li class="nav-item"><a class="nav-link" href="/transactions">Dashboard</a></li>
			
			@endif
			@else
      <li class="nav-item active">
				<a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
				<a class="nav-link" href="/register">Register</a>
      </li>
      <li class="nav-item">
				<a class="nav-link" href="/login">Login</a>
			</li>
			@endif
			<li class="nav-item"><a class="nav-link" href="/properties">Properties</a></li>
    </ul>
  </div>
</nav>


			{{-- <main class="py-4"> --}}
				@yield("content")
			{{-- </main> --}}

	</div>

	{{-- Footer --}}



	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

	<!-- Popper -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>

	<!-- Bootstrap js CDN -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

	<script type="text/javascript" src="../assets/scripts/script.js"></script>

</body>
</html>