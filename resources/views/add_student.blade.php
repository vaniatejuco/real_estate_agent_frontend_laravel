@extends('layouts.template')

@section("title", "Subject Catalog")
@section('content')

	<div class="container py-4">
		<div class="row py-4">
			<div class="col-lg-6 offset-lg-2">
				<h1>Add New Student</h1>
				<form action="/student/new" method="POST" enctype="multipart/form-data">
					@csrf
					<label for="name">Student Name:</label>
					<input type="text" name="name" id="name" class="form-control">

					<label for="year_level">Year Level:</label>
					<input type="text" name="year_level" id="year_level" class="form-control">

					<label for="subject1">Subject 1:</label>
					<input type="text" name="subject1" id="subject1" class="form-control">

					<label for="subject2">Subject 2:</label>
					<input type="text" name="subject2" id="subject2" class="form-control">

					<label for="subject3">Subject 3:</label>
					<input type="text" name="subject3" id="subject3" class="form-control">

					<label for="gwa">General Weighted Average (GWA):</label>
					<input type="number" name="gwa" id="gwa" class="form-control">

					<button type="submit" class="btn btn-primary mt-3" id="submit">Add Student</button>
				</form>
			</div>
		</div>
	</div>


@endsection