@extends('layouts.template')

@section("title", "Update Student Info")
@section('content')

	<div class="container py-4">
		<div class="row py-4">
			<div class="col-lg-6 offset-lg-2">
				<h1>Student Info</h1>
				<form action="/student/edit/{{ $student->_id }}" method="POST" enctype="multipart/form-data">
					{{ method_field("PUT") }}
					@csrf
					<label for="name">Student Name:</label>
					<input type="text" name="name" id="name" class="form-control" value="{{ $student->name }}">

					<label for="year_level">Year Level:</label>
					<input type="text" name="year_level" id="year_level" class="form-control" value="{{ $student->year_level }}">

					<label for="subject1">Subject 1:</label>
					<input type="text" name="subject1" id="subject1" class="form-control" value="{{ $student->subject1 }}">

					<label for="subject2">Subject 2:</label>
					<input type="text" name="subject2" id="subject2" class="form-control" value="{{ $student->subject2 }}">

					<label for="subject3">Subject 3:</label>
					<input type="text" name="subject3" id="subject3" class="form-control" value="{{ $student->subject3 }}">

					<label for="gwa">General Weighted Average (GWA):</label>
					<input type="number" name="gwa" id="gwa" class="form-control" value="{{ $student->gwa }}">
{{-- 
					<button type="submit" class="btn btn-info mt-3" id="submit">Update Student Info</button> --}}
				</form>
			</div>
		</div>
	</div>


@endsection