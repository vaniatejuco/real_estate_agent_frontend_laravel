@extends('layouts.template')

@section("title", "Subject Catalog")
@section('content')

	<div class="container py-4" style="margin-left: 210px;">
		<h1 class="text-center">Student List</h1>
		<form action="/filterSubject" method="POST">
			@csrf
			<h3>Filter by Subject</h3>
			<input type="text" name="filter" id="filter" class="form-control">
			<button type="submit" class="btn btn-secondary mt-3" id="submit"><i class="fas fa-filter"></i> Filter</button>
		</form>

		<div class="row py-2">
			<table class="table table-striped mt-3">
				<thead>
					<th>Student Name</th>
					<th>Year Level</th>
					<th>Subject 1</th>
					<th>Subject 2</th>
					<th>Subject 3</th>
					<th>GWA</th>
					<th> </th>
				</thead>
				<tbody>
				@foreach($studentslist as $indiv_student)
					<tr>
						<td>{{ $indiv_student->name }}</td>
						<td>{{ $indiv_student->year_level }}</td>
						<td>{{ $indiv_student->subject1 }}</td>
						<td>{{ $indiv_student->subject2 }}</td>
						<td>{{ $indiv_student->subject3 }}</td>
						<td>{{ $indiv_student->gwa }}</td>
						<td>
							<a href="/student/edit/{{ $indiv_student->_id }}" class="btn btn-outline-info">Student Profile</a>
							<a href="/delete/{{ $indiv_student->_id }}" class="btn btn-danger">Delete</a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>


@endsection