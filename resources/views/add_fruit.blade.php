<!DOCTYPE html>
<html>
<head>
	<title>Fruit Basket</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>@yield("title")</title>

	{{-- Fonts --}}
	<link href="https://fonts.googleapis.com/css?family=Cinzel|Montserrat" rel="stylesheet">

	{{-- Font Awesome --}}
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	{{-- Bootstrap --}}
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


	{{-- Custom CSS --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>
	<div class="container py-4">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<h1>Add New Fruit</h1>
				<form action="/fruit/new" method="POST" enctype="multipart/form-data">
					@csrf
					<label for="name">Fruit Name:</label>
					<input type="text" name="name" id="name" class="form-control">

					<label for="price">Price</label>
					<input type="number" name="price" id="price" class="form-control">

					<label for="color">Color:</label>
					<input type="text" name="color" id="color" class="form-control">

					<label for="taste">Taste:</label>
					<input type="text" name="taste" id="taste" class="form-control">


					<button type="submit" class="btn btn-success mt-3" id="submit">Add New Fruit</button>
				</form>
			</div>
		</div>
	</div>


</body>
</html>