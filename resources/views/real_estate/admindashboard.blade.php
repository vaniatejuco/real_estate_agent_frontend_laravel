@extends('layouts.template1')

@section("title", "Dashboard")
@section('content')

	<div class="container py-4">
		<div class="container my-3">

			<a href="/newproperty" class="btn btn-success">Add New Property</a>
			<a href="/newevent" class="btn btn-success">Add New Event</a>
			
		</div>
		@if(Session::has("message"))
			<div class="alert alert-success">
				{{Session::pull('message')}}

			</div>
			<h1 class="text-center">Pending Transactions</h1>
		@endif


		<div class="row py-2">
			<table class="table table-striped mt-3">
				<thead>
					<th>Property</th>
					<th>Event</th>
					<th>Name</th>
					<th>Contact Number</th>
					<th>Status</th>
					<th></th>

				</thead>
				<tbody>
				@foreach($pendinglist as $indiv_transaction)

					<tr>
						<td><a href="/pinfo/{{ $indiv_transaction->property }}">{{ $indiv_transaction->property }}</a></td>
						<td><a href="/einfo/{{ $indiv_transaction->event }}"> {{$indiv_transaction->event }}</a></td>
						<td>{{ $indiv_transaction->user->name }}</td>
						<td>{{ $indiv_transaction->user->contact_number }}</td>
						<td>{{ $indiv_transaction->status }}</td>
                        <td>
							@if($indiv_transaction->status = "pending")
								<form action="/confirm" method="POST">
									@csrf
									<input type="text" name="id" value="{{$indiv_transaction->_id}}" hidden>
									<button type="submit" class="btn btn-primary">Confirm</button>
								</form>
							@endif
                           
                        </td>

					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>


@endsection