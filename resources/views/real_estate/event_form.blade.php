@extends('layouts.template1')

@section("title", "New Property")
@section('content')


    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-2"></div>
            <div class="col-lg-6 col-md-8 login-box my-5">

                <div class="col-lg-12 login-title text-center">
                   <h3> SCHEDULE A TRIP </h3>
                </div>

                <div class="col-lg-12 login-form">
                    <div class="col-lg-12 login-form">
                        <form action="/newevent" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="form-control-label">DATE</label>
                                <input type="text" name="date" class="form-control" placeholder="YYYY-MM-DD">
                            </div>
                            <div class="form-group">
                                <select class="form-control form-control-md" name="property">
                                @foreach ($propertylist as $property)
                                <option value="{{$property->_id}}">{{$property->name}}</option>
                                    
                                @endforeach
                                </select>
                            </div>
                      

                            <div class="col-lg-12 loginbttm">
                                <div class="col-lg-6 login-btm login-text">
                                    <!-- Error Message -->
                                </div>
                                <div class="col-lg-6 login-btm login-button">
                                    <button type="submit" class="btn btn-outline-primary">SCHEDULE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2"></div>
            </div>
        </div>








@endsection