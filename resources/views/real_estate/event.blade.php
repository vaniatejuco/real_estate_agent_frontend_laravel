@extends('layouts.template1')

@section("title", "Properties")
@section('content')

	<div class="container py-4" >
		<h1 class="text-center">Trip Info</h1>
		@if (count($eventlist) < 1)
			<div class="text-center"><h4>Sorry there are no scheduled trips for this property at the moment.</h4></div>
		@else 
			<h2 class="text-center">{{ $eventlist[0]->property->name}}</h2>


			<div class="row py-2">
				<table class="table table-striped mt-3">
					<thead>
						<th>Date</th>
						<th> </th>
					</thead>
					<tbody>
					@foreach($eventlist as $indiv_event)
						<tr>
							<td><?php
								$timestamp=  strtotime($indiv_event->date);
								echo date("F j, Y",$timestamp); ?>

							</td>



							@if (Session::has('role'))
								@if (Session::get('role') === 1)
								<td>
									<a href="/event/delete/{{ $indiv_event->_id }}" class="btn btn-danger">Delete</a>
								</td>
								@else
								<td>
									<form action="/book" method="post">
										@csrf

										<input type="text" name="token" value="{{Session::get('token')}}" hidden>
										<input type="text" name="user_id" value="{{Session::get('user_id')}}" hidden>
										<input type="text" name="property_id" value="{{$property_id}}" hidden>
										<input type="text" name="event_id" value="{{$indiv_event->_id}}" hidden>
										<button class="btn btn-outline-warning" type="submit">Book FREE Now</button>
									</form>					
								</td>
								@endif
							@else
								<td>
									<a href="/login" class="btn btn-outline-warning">Book FREE Now</a>								
								</td>
							@endif
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		@endif
	</div>


@endsection