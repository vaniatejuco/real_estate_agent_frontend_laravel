@extends('layouts.template1')

@section("title", "Real Estate Agent")
@section('content')
<div class="container-fluid" id="home">
	
	<div class="container py-3" id="card">
		<div class="row">
			<div class="col-lg-6">
				<img src="{{asset('img/juan.jpg')}}" height="600" width="450" alt="agent">
			</div>
			<div class="col-lg-6 py-5">
				<div id="intro">
					<h1>Hi! This is Juan Cruz,</h1>
					<h2 class="text-warning">Your Real Estate Agent</h2>
					<p>Let me show you around the most sought after properties right now.</p>
					<p>We have properties all over the Philippines.</p>
					<p>Transactions are <span class="bold  text-warning">HASSLE-FREE with our NEW Online Booking System</span>.</p>
					<p>You can now book property trips for <span class="bold">FREE</span>. You have <span>UNLIMITED FREE</span> trips on all our available properties!</p>
					<button class="btn btn-warning"><a href="/properties">Book Your FREE Trips Today!</a></button>
	
				</div>
	
			</div>
		</div>
	</div>
</div>




@endsection