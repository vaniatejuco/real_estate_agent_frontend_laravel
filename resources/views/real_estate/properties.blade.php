@extends('layouts.template1')

@section("title", "Properties")
@section('content')

	<div class="container py-4" >
		<h1 class="text-center">Available Properties</h1>


		<div class="row py-2">
			<table class="table table-striped mt-3">
				<thead>
					<th>Name</th>
					<th>Address</th>
					<th>Features</th>
					<th>Amenities</th>
					<th>Landmarks</th>
					<th>TCP</th>
					<th>Monthly</th>
					<th> </th>
				</thead>
				<tbody>
				@foreach($propertylist as $indiv_property)
					<tr>
						<td>{{ $indiv_property->name }}</td>
						<td>{{ $indiv_property->address }}</td>
						<td>{{ $indiv_property->features }}</td>
						<td>{{ $indiv_property->amenities }}</td>
						<td>{{ $indiv_property->landmarks }}</td>
						<td>{{ $indiv_property->total_price }}</td>
                        <td>{{ $indiv_property->monthly }}</td>
                        <td>
                            <a href="/event/{{ $indiv_property->_id }}" class="btn btn-outline-info">See Trip Schedule</a>
                        </td>

                        @if (isset($token))
                            @if ($token.role === 1)
                            <td>
                                <a href="/property/edit/{{ $indiv_property_id }}" class="btn btn-outline-info">Student Profile</a>
                                <a href="/property/delete/{{ $indiv_property->_id }}" class="btn btn-danger">Delete</a>
                            </td>
                            @else
                            @endif
                        @endif
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>


@endsection