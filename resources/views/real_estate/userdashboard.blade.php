@extends('layouts.template1')

@section("title", "Dashboard")
@section('content')

	<div class="container py-4">
		<h1 class="text-center">Your Transactions</h1>


		<div class="row py-2">
			<table class="table table-striped mt-3">
				<thead>
					<th>Date</th>
					<th>Property</th>
					<th>Status</th>
					<th></th>

				</thead>
				<tbody>
				@foreach($transactionlist as $indiv_transaction)

					<tr>
					@if($indiv_transaction->event === null)	
						<td>Date No Longer Available</td>
					@else
						<td>{{ $indiv_transaction->event->date }}</td>
					@endif
						<td>{{ $indiv_transaction->property }}</td>
						<td>{{ $indiv_transaction->status }}</td>
                        <td>
							@if($indiv_transaction->status === "pending")
								<p>Please wait for a call within the day for us to confirm your booking.</p>
							@endif
                           
                        </td>

					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>


@endsection