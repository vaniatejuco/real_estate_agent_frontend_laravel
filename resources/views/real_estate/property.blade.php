@extends('layouts.template1')

@section("title", "Properties")
@section('content')

	<div class="container py-4">
		<h1 class="text-center">Property Info</h1>


		<div class="row py-2">
			<table class="table table-striped mt-3">
				<thead>
					<th>Name</th>
					<th>Address</th>
					<th>Features</th>
					<th>Amenities</th>
					<th>Landmarks</th>
					<th>TCP</th>
					<th>Monthly</th>
					<th> </th>
				</thead>
				<tbody>
				@foreach ($propertylist as $property)
					
				
					<tr>
						<td>{{ $property->name }}</td>
						<td>{{ $property->address }}</td>
						<td>{{ $property->features }}</td>
						<td>{{ $property->amenities }}</td>
						<td>{{ $property->landmarks }}</td>
						<td>{{ $property->total_price }}</td>
                        <td>{{ $property->monthly }}</td>


                 
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>


@endsection