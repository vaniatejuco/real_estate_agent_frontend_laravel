@extends('layouts.template1')

@section("title", "New Property")
@section('content')


    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-2"></div>
            <div class="col-lg-6 col-md-8 login-box my-5">

                <div class="col-lg-12 login-title text-center">
                   <h3> NEW PROPERTY </h3>
                </div>

                <div class="col-lg-12 login-form">
                    <div class="col-lg-12 login-form">
                        <form action="/newproperty" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="form-control-label">PROJECT NAME</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">ADDRESS</label>
                                <input type="text" name="address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">FEATURES</label>
                                <input type="text" name="features" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">AMENITIES</label>
                                <input type="text" name="amenities" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">LANDMARKS</label>
                                <input type="text" name="landmarks" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">TOTAL CONTRACT PRICE</label>
                                <input type="number" name="tcp" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">MONTHLY AMORTIZATION</label>
                                <input type="number" name="monthly" class="form-control" >
                            </div>

                            <div class="col-lg-12 loginbttm">
                                <div class="col-lg-6 login-btm login-text">
                                    <!-- Error Message -->
                                </div>
                                <div class="col-lg-6 login-btm login-button">
                                    <button type="submit" class="btn btn-outline-primary">ADD</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2"></div>
            </div>
        </div>








@endsection