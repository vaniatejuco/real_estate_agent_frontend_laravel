<!DOCTYPE html>
<html>
<head>
	<title>Fruit Basket</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>@yield("title")</title>

	{{-- Fonts --}}
	<link href="https://fonts.googleapis.com/css?family=Cinzel|Montserrat" rel="stylesheet">

	{{-- Font Awesome --}}
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	{{-- Bootstrap --}}
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


	{{-- Custom CSS --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>
	<div class="container py-4">
		<h1 class="text-center">Fruit Basket</h1>
		<div class="row py-2">
			@foreach($fruitlist as $indiv_fruit)
			<div class="col-lg-4 mt-3">
				<div class="card">
					<img src="#" class="card-img-top" style="height: 50px">
					<div class="card-body">
						<h3 class="card-title"><strong>{{ $indiv_fruit->name }}</strong></h3>
						<p>Price: <strong>{{ $indiv_fruit->price }}</strong></p>
						<p>Color: <strong>{{ $indiv_fruit->color }}</strong></p>
						<p>Taste: <strong>{{ $indiv_fruit->taste }}</strong></p>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>


</body>
</html>